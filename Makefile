app@init:
	docker-compose build
api@init:
	- docker-compose build --no-cache
	- docker run -t -v "${PWD}/django/:/api" django /bin/bash -c 'django-admin startproject pyges && cd ./pyges && django-admin startapp src'
client@init:
	docker-compose exec client npm i

client@up:
	docker-compose up -d client
django@up:
	docker-compose up -d django

stop:
	docker-compose stop	
flask_logs:
	docker-compose logs --tail=50 -f django
client_logs:
	docker-compose logs --tail=50 -f logs
